package netmind.es.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {


    private static final String KEYSTRING = "a";
    private String saludo;
    private Button btn;
    private EditText editText;
    private BlankFragment.Comunicacion comunicacion;

    interface Comunicacion{
        void datosEnvio(String dato);
    }

    public BlankFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance(String saludo){
        BlankFragment blankFragment = new BlankFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEYSTRING,saludo);
        blankFragment.setArguments(bundle);
        return blankFragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            comunicacion = (BlankFragment.Comunicacion) context;
        }catch (ClassCastException e){
           e.printStackTrace();
            Log.e("fragment","la hemos cagao");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            saludo = getArguments().getString(KEYSTRING);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout= inflater.inflate(R.layout.fragment_blank, container, false);
        bindView(layout);
        settingViews();


        return layout;
    }

    private void settingViews() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editText.length()>0){
                    comunicacion.datosEnvio(editText.getText().toString());
                }else{
                    comunicacion.datosEnvio("Anda ves y escribe en Fragment");
                }
            }
        });
    }

    private void bindView(View layout) {
        btn = (Button) layout.findViewById(R.id.fragment_btn);
        editText = (EditText) layout.findViewById(R.id.fragment_et_text);
    }

}
